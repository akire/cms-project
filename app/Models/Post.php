<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Post extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'body', 'post_image', 'user_id'];

    public function path()
    {
    	return '/admin/posts/' . $this->id;
    }

    public function user()
    {
    	return $this->belongsTo(User::class)->withTrashed();
    }


    public function getPostImageAttribute($value)
    {
        if(strpos($value, 'https://') !== FALSE || strpos($value, 'http://') !== FALSE) {
            return $value;
        }
        
        return $value ? asset('storage/'.$value) : '';
    }
}
