<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\Models\Role;
use App\Models\User;

class Permission extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'slug'];
    
    public function roles()
    {
    	return $this->belongsToMany(Role::class);
    }

    public function users()
    {
    	return $this->belongsToMany(User::class);
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = Str::ucfirst($value);
    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = Str::of(Str::lower($value))->slug('-');
    }
}
