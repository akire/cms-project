<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Permission;
use App\Models\Role;

class PermissionController extends Controller
{
    //
    public function index()
    {
    	return view('admin.permissions.index', ['permissions' => Permission::all()]);
    }

    public function store(Request $request)
    {
    	$attributes = request()->validate([
    		'name' => 'required|max:100|unique:permissions'
    	]);

    	$attributes['slug'] = strtolower($attributes['name']);

    	Permission::create($attributes);
    	session()->flash('message', 'New Permission added');

    	return back();

    }

    public function edit(Permission $permission)
    {
    	return view('admin.permissions.edit', ['permission' => $permission, 'roles' => Role::all()]);
    }

    public function update(Permission $permission)
    {
    	$attributes = request()->validate([
    		'name' => 'required|max:100|unique:permissions,name,'.$permission->id
    	]);

    	$permission->name = ucfirst($attributes['name']);
    	$permission->slug = strtolower($attributes['name']);

    	if($permission->isDirty('name')){
    		$permission->save();
    		session()->flash('message', 'Permission updated');
    	} else {
    		session()->flash('message', 'Nothing to update');
    	}

    	return back();
    }

    public function destroy(Permission $permission)
    {
    	$permission->delete();
    	session()->flash('message', 'Permission deleted');
    	return back();
    }
}
