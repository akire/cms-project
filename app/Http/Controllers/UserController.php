<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;

class UserController extends Controller
{
    //
    public function index()
    {
        $users = User::paginate(10);
        return view('admin.users.index', ['users' => $users]);
    }

    public function show(User $user)
    {
    	return view('admin.users.profile', [ 'user' => $user, 'roles' => Role::all() ]);
    }

    public function update(User $user)
    {

    	$attributes = request()->validate([
            'username' => 'required|min:4|max:50',
            'name' => 'required|max:255',
            'avatar' => 'mimes:jpeg,png|file',
            'email' => 'required|max:255'
            // 'password' => 'min:6|max:255|confirmed'
        ]);
        // dd($attributes);
        

        if(request('avatar')) {
            $attributes['avatar'] = request('avatar')->store('avatars');
        }

        $user->update($attributes);
        session()->flash('message', 'Profile updated');

        return back();
    }

    public function attach(User $user)
    {

        $user->roles()->attach(request('role'));
        session()->flash('message', 'User Access added');

        return back();
    }

    public function detach(User $user)
    {

        $user->roles()->detach(request('role'));
        session()->flash('message', 'User Access removed');

        return back();
    }

    public function destroy(User $user)
    {
        $user->delete();
        session()->flash('message', 'User deleted');

        return back();
    }
}
