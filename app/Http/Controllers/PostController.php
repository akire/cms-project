<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use Illuminate\Support\Facades\Session;

class PostController extends Controller
{
    //
    public function index()
    {
        $posts = auth()->user()->posts()->paginate(5);
        // $posts = Post::all();
        return view('admin.posts.index', ['posts' => $posts]);
    }

    public function show(Post $post)
    {
        // dd($post->user);
    	return view('admin.posts.blog-post', ['post' => $post]);
    }

    public function create()
    {
        $this->authorize('create', Post::class);
    	return view('admin.posts.create');
    }

    public function store()
    {
        $this->authorize('create', Post::class);

    	$inputs = request()->validate([
    		'title' => 'required|min:8|max:255',
    		'post_image' => 'mimes:jpeg,png|file',
    		'body' => 'required'
    	]);

    	if(request('post_image')) {
    		$inputs['post_image'] = request('post_image')->store('post_images');
    	}

    	auth()->user()->posts()->create($inputs);

        session()->flash('message', 'New Post created: '. $inputs['title']);

    	return redirect()->route('posts.index');

    }

    public function edit(Post $post)
    { 
        $this->authorize('view', $post);
        return view('admin.posts.edit', ['post' => $post]);
    }

    public function update(Post $post)
    {
        $attributes = request()->validate([
            'title' => 'required|min:8|max:255',
            'post_image' => 'mimes:jpeg,png|file',
            'body' => 'required'
        ]);

        if(request('post_image')) {
            $attributes['post_image'] = request('post_image')->store('post_images');
        }

        $this->authorize('update', $post);

        $post->update($attributes);
        session()->flash('message', 'Post updated');

        return redirect()->route('posts.index');

    }

    public function destroy(Post $post)
    {
        $this->authorize('delete', $post);
        $post->delete();
        Session::flash('message', 'Post was deleted');

        return back();
    }
}
