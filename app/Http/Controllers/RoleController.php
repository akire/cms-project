<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\Permission;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        return view('admin.roles.index', ['roles' => Role::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $attributes = request()->validate([
            'name' => 'required|unique:roles|max:100'
        ]);

        $attributes['slug'] = strtolower($attributes['name']);
        // dd($attributes);

        Role::create($attributes);

        session()->flash('message', 'New Role added');
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        //
        return view('admin.roles.edit', ['role' => $role, 'permissions' => Permission::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        //
        $attributes = request()->validate([
            'name' => 'required|unique:roles,name,'.$role->id.'|max:100'
        ]);

        $role->name = $attributes['name'];
        $role->slug = strtolower($attributes['name']);
        // $attributes['slug'] = strtolower($attributes['name']);
        // $role->update($attributes);
        if($role->isDirty('name')){
            session()->flash('message', 'Role updated');
            $role->save();
        }else{
            session()->flash('message', 'Nothing to update');
        }

        return back();
    }

    public function attach(Role $role)
    {
        $role->permissions()->attach(request('permission'));
        return back();
    }

    public function detach(Role $role)
    {
        $role->permissions()->detach(request('permission'));
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        //
        $role->delete();
        session()->flash('message', 'Role deleted');

        return redirect()->route('roles.index',  ['roles' => Role::all()]);
    }
}
