<x-admin-master>
  
  @section('content')
  	@if(auth()->user()->userHasRole('Admin'))
	    <h1>Dashboard {{ auth()->user()->name }}</h1>
	@endif
  @endsection

</x-admin-master>