<x-admin-master>
	@section('content')
		<h1>Edit Role: {{ $role->name }}</h1>
		@if(session('message'))
			<div class = "alert alert-success">{{ session('message') }}</div>
		@endif
		
		<div class = "row">
			<div class = "col-sm-4">
				<form action = "{{ route('roles.update', $role) }}" method = "POST">
					@csrf
					@method('PUT')
					<div class = "form-group">
						<label for = "name">Name</label>
						<input type = "text" name = "name" class = "form-control" value = "{{ $role->name }}" required>
						@error('name')
							<span><strong>{{ $message }}</strong></span>
						@enderror
					</div>
					<button type = "submit" class = "btn btn-primary">Submit</button>
				</form>
			</div>

			<div class = "col-sm-8">

				<div class="card shadow mb-4">
		            <div class="card-header py-3">
		              <h6 class="m-0 font-weight-bold text-primary">Permissions</h6>
		            </div>
		            <div class="card-body">
		              <div class="table-responsive">
		                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
		                  <thead>
		                    <tr>
		                      <th></th>
		                      <th>Id</th>
							  <th>Name</th>
							  <th>Slug</th>
							  <th>Created At</th>
							  <th width = "10%">Edit</th>
							  <th width = "10%">Delete</th>
		                    </tr>
		                  </thead>
		                  <tfoot>
		                    <tr>
		                      <th></th>
		                      <th>Id</th>
							  <th>Name</th>
							  <th>Slug</th>
							  <th>Created At</th>
							  <th width = "10%">Edit</th>
							  <th width = "10%">Delete</th>
		                    </tr>
		                  </tfoot>
		                  <tbody>
		                  	@forelse($permissions as $permission)
								<tr>
									<td>
										<input type = "checkbox" class = "form-control"
										@if($role->permissions->contains($permission))
											checked
										@endif
										>
									</td>
									<td>{{ $permission->id }}</td>
									<td>{{ $permission->name }}</td>
									<td>{{ $permission->slug }}</td>
									<td>{{ $permission->created_at->diffForHumans() }}</td>
									<td>
			                        	<form action = "{{ route('role.permission.attach', $role) }}" method = "POST">
			                        		@csrf
			                        		@method('PUT')
			                        		<input type = "hidden" name = "permission" value = "{{ $permission->id }}">
			                        		<button type = "submit" class = "btn btn-primary"

			                        			@if($role->permissions->contains($permission))
					                      			disabled
					                      		@endif

			                        		>Attach</button>

			                        	</form>

			                        </td>
			                        <td>
			                        	<form action = "{{ route('role.permission.detach', $role) }}" method = "POST">
			                        		@csrf
			                        		@method('PUT')
			                        		<input type = "hidden" name = "permission" value = "{{ $permission->id }}">
			                        		<button type = "submit" class = "btn btn-danger"

			                        		@if(!$role->permissions->contains($permission))
				                      			disabled
				                      		@endif

					                      	>Detach</button>

			                        	</form>
			                        </td>
								</tr>
							@empty
								<tr><td colspan = "6"></td></tr>
							@endforelse
		                  </tbody>
		                </table>
		              </div>
		            </div>
		          </div>

			</div>

		</div>
	@endsection
</x-admin-master>