<x-admin-master>
	@section('content')
		<h1>Roles</h1>
			@if(session('message'))
				<div class = "alert alert-success">{{ session('message') }}</div>
			@endif
		<div class = "row">
			<div class = "col-sm-3">
				<form action = "{{ route('roles.store') }}" method = "POST">
					@csrf
					<div class = "form-group">
						<label for = "name">Name</label>
						<input type = "text" name = "name" class = "form-control" value = "{{ old('name') }}" required>
						@error('name')
							<span><strong>{{ $message }}</strong></span>
						@enderror
					</div>
					<button type = "submit" class = "btn btn-primary">Submit</button>
				</form>
			</div>

			<div class = "col-sm-9">
				<div class="card shadow mb-4">
		            <div class="card-header py-3">
		              <h6 class="m-0 font-weight-bold text-primary">Roles</h6>
		            </div>
		            <div class="card-body">
		              <div class="table-responsive">
		                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
		                  <thead>
		                    <tr>
		                      <th>Id</th>
							  <th>Name</th>
							  <th>Slug</th>
							  <th>Created At</th>
							  <th width = "10%">Edit</th>
							  <th width = "10%">Delete</th>
		                    </tr>
		                  </thead>
		                  <tfoot>
		                    <tr>
		                      <th>Id</th>
							  <th>Name</th>
							  <th>Slug</th>
							  <th>Created At</th>
							  <th width = "10%">Edit</th>
							  <th width = "10%">Delete</th>
		                    </tr>
		                  </tfoot>
		                  <tbody>
		                  	@foreach($roles as $role)
								<tr>
									<td>{{ $role->id }}</td>
									<td>{{ $role->name }}</td>
									<td>{{ $role->slug }}</td>
									<td>{{ $role->created_at->diffForHumans() }}</td>
									<td><a href = "{{ route('roles.edit', $role) }}" class = "btn btn-warning btn-block">Edit</a></td>
									<td>
										<form action = "{{ route('roles.destroy', $role) }}" method = "POST">
											@csrf
											@method('DELETE')
											<button type = "submit" class = "btn btn-danger btn-block">Delete</button>
										</form>
									</td>
								</tr>
							@endforeach
		                  </tbody>
		                </table>
		              </div>
		            </div>
		          </div>


			</div>


		</div>
	@endsection
</x-admin-master>