<x-admin-master>
	@section('content')
		<h1>Create</h1>

		<form method = "POST" action = "{{ route('post.store') }}" enctype = "multipart/form-data">
			@csrf
			<div class = "form-group">
				<label for = "title">Title</label>
				<input type = "text" name = "title" class = "form-control" placeholder = "Title" required>
			</div>

			@error('title')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror

			<div class = "form-group">
				<label for = "file">File</label>
				<input type = "file" name = "post_image" class = "form-control-file">
			</div>

			<div class = "form-group">
				<label for = "body">Body</label>
				<textarea name = "body" cols = "25" rows = "10" class = "form-control" required></textarea>
			</div>
			<button type = "submit" class = "btn btn-primary">Submit</button>
		</form>

	@endsection

</x-admin-master>