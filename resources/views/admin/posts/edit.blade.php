<x-admin-master>
	@section('content')
		<h1>Edit Post</h1>

		<form method = "POST" action = "{{ route('post.update', $post->id) }}" enctype = "multipart/form-data">
			@csrf
			@method('PATCH')

			<div class = "form-group">
				<label for = "title">Title</label>
				<input type = "text" name = "title" value = "{{ $post->title }}" class = "form-control" placeholder = "Title" required>
			</div>

			{{-- @errors('title')
				{{ $message }}
			@enderrors --}}
			<div class = "form-group">
				<div>
					<img class = "img-responsive" height = "100px" src = "{{ $post->post_image }}">
				</div>
				<label for = "file">File</label>

				<input type = "file" name = "post_image" class = "form-control-file">
			</div>

			<div class = "form-group">
				<label for = "body">Body</label>
				<textarea name = "body" cols = "25" rows = "10" class = "form-control" required>{{ $post->body }}</textarea>
			</div>
			<button type = "submit" class = "btn btn-primary">Update</button>
		</form>

	@endsection

</x-admin-master>