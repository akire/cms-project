<x-admin-master>
	@section('content')
		<h1>My Profile {{ $user->name }}</h1>

		@if(session('message'))
			<div class = "alert alert-success">{{ session('message') }}</div>
		@endif

		<div class = "row">
			<div class = "col-sm-6">
				<form action = "{{ route('user.profile.update', $user) }}" method = "POST" enctype = "multipart/form-data">
					@csrf
					@method('PUT')
					<div>
						<img src = "{{ asset($user->avatar) }}" height = "80px"  width = "80px" class = "img-profile rounded-circle">
					</div>

					<div class ="form-group">
						<input type = "file" name = "avatar">
						@error('avatar')
			                <span class="invalid-feedback" role="alert">
			                    <strong>{{ $message }}</strong>
			                </span>
			            @enderror
					</div>

					<div class ="form-group">
						<label for = "username">Username</label>
						<input type = "text" name = "username" id = "username" value = "{{ $user->username }}" class = "form-control" >

						@error('username')
			                <span class="invalid-feedback" role="alert">
			                    <strong>{{ $message }}</strong>
			                </span>
			            @enderror
					</div>

					<div class ="form-group">
						<label for = "name">Name</label>
						<input type = "text" name = "name" value = "{{ $user->name }}" class = "form-control" required>

						@error('name')
			                <span class="invalid-feedback" role="alert">
			                    <strong>{{ $message }}</strong>
			                </span>
			            @enderror
					</div>

					<div class ="form-group">
						<label for = "email">Email</label>
						<input type = "text" name = "email" value = "{{ $user->email }}" class = "form-control" required>

						@error('email')
			                <span class="invalid-feedback" role="alert">
			                    <strong>{{ $message }}</strong>
			                </span>
			            @enderror
					</div>

					<div class ="form-group">
						<label for = "password">Password</label>
						<input type = "password" name = "password" class = "form-control" >

						@error('password')
			                <span class="invalid-feedback" role="alert">
			                    <strong>{{ $message }}</strong>
			                </span>
			            @enderror
					</div>

					<div class ="form-group">
						<label for = "password_confirmation">Confirm Password</label>
						<input type = "password" name = "password_confirmation" class = "form-control" >

						@error('password_confirmation')
			                <span class="invalid-feedback" role="alert">
			                    <strong>{{ $message }}</strong>
			                </span>
			            @enderror
					</div>

					<button type = "submit" class = "btn btn-primary">Submit</button>
				</form>
			</div>
		</div>

		<hr>
		<div class = "row">
			<div class = "col-sm-12">
				<div class="card shadow mb-4">
					<div class="card-header py-3">
		              <h6 class="m-0 font-weight-bold text-primary">Roles</h6>
		            </div>
					<div class="card-body">
						<div class="table-responsive">
			                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
			                  <thead>
			                    <tr>
			                      <th></th>
			                      <th>Id</th>
			                      <th>Name</th>
			                      <th>Slug</th>
			                      <th>Attach</th>
			                      <th>Detach</th>
			                    </tr>
			                  </thead>
			                  <tfoot>
			                    <tr>
			                      <th></th>
			                      <th>Id</th>
			                      <th>Name</th>
			                      <th>Slug</th>
			                      <th>Attach</th>
			                      <th>Detach</th>
			                    </tr>
			                  </tfoot>
			                  <tbody>
			                  	@foreach($roles as $role)
			                      <tr>
			                      	<td>
			                      		<input type = "checkbox" 
			                      		@if($user->roles->contains($role))
			                      			checked
			                      		@endif
			                      		{{-- @foreach($user->roles as $userRole)
			                      			@if($userRole->id == $role->id)
			                      				checked
			                      			@endif 
			                      		@endforeach --}}
			                      		{{-- {{ in_array($role->name, $user->roles()) ? 'checked' : '' }} --}}
			                      		></td>
			                        <td>{{ $role->id }}</td>
			                        <td>{{ $role->name }}</td>
			                        <td>{{ $role->slug }}</td>
			                        <td>
			                        	<form action = "{{ route('user.role.attach', $user) }}" method = "POST">
			                        		@csrf
			                        		@method('PUT')
			                        		<input type = "hidden" name = "role" value = "{{ $role->id }}">
			                        		<button type = "submit" class = "btn btn-primary"

			                        			@if($user->roles->contains($role))
					                      			disabled
					                      		@endif

			                        		>Attach</button>

			                        	</form>

			                        </td>
			                        <td>
			                        	<form action = "{{ route('user.role.detach', $user) }}" method = "POST">
			                        		@csrf
			                        		@method('PUT')
			                        		<input type = "hidden" name = "role" value = "{{ $role->id }}">
			                        		<button type = "submit" class = "btn btn-danger"

			                        		@if(!$user->roles->contains($role))
				                      			disabled
				                      		@endif

					                      	>Detach</button>

			                        	</form>
			                        </td>
			                      </tr>

			                    @endforeach
			                  </tbody>
			                </table>
			            </div>
			        </div>
			    </div>
			       
			</div>
		</div>

	@endsection
</x-admin-master>