<?php

use Illuminate\Support\Facades\Route;

Route::get('/permissions', [App\Http\Controllers\PermissionController::class, 'index'])->name('permissions.index');
Route::post('/permissions', [App\Http\Controllers\PermissionController::class, 'store'])->name('permissions.store');
Route::get('/permissions/{permission}/edit', [App\Http\Controllers\PermissionController::class, 'edit'])->name('permissions.edit');
Route::put('/permissions/{permission}/update', [App\Http\Controllers\PermissionController::class, 'update'])->name('permissions.update');
Route::delete('/permissions/{permission}', [App\Http\Controllers\PermissionController::class, 'destroy'])->name('permissions.destroy');

Route::put('/permissions/{permission}/roles/attach', [App\Http\Controllers\PermissionController::class, 'attach'])->name('permission.role.attach');
Route::put('/permissions/{permission}/roles/detach', [App\Http\Controllers\PermissionController::class, 'detach'])->name('permission.role.detach');

