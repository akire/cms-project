<?php

use Illuminate\Support\Facades\Route;

Route::get('/roles', [App\Http\Controllers\RoleController::class, 'index'])->name('roles.index');
Route::post('/roles', [App\Http\Controllers\RoleController::class, 'store'])->name('roles.store');
Route::get('/roles/{role}/edit', [App\Http\Controllers\RoleController::class, 'edit'])->name('roles.edit');
Route::put('/roles/{role}/update', [App\Http\Controllers\RoleController::class, 'update'])->name('roles.update');
Route::delete('/roles/{role}', [App\Http\Controllers\RoleController::class, 'destroy'])->name('roles.destroy');

Route::put('/roles/{role}/permissions/attach', [App\Http\Controllers\RoleController::class, 'attach'])->name('role.permission.attach');
Route::put('/roles/{role}/permissions/detach', [App\Http\Controllers\RoleController::class, 'detach'])->name('role.permission.detach');
