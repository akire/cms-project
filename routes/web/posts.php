<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['auth'])->group(function() {
	Route::get('/posts', [App\Http\Controllers\PostController::class, 'index'])->name('posts.index');
	Route::post('/posts', [App\Http\Controllers\PostController::class, 'store'])->name('post.store');
	Route::get('/posts/create', [App\Http\Controllers\PostController::class, 'create'])->name('post.create');
	Route::get('/posts/{post}/edit', [App\Http\Controllers\PostController::class, 'edit'])->name('post.edit');
	Route::patch('/posts/{post}', [App\Http\Controllers\PostController::class, 'update'])->name('post.update');
	Route::delete('/posts/{post}', [App\Http\Controllers\PostController::class, 'destroy'])->name('post.destroy');
});
Route::get('/posts/{post}', [App\Http\Controllers\PostController::class, 'show'])->name('post');

